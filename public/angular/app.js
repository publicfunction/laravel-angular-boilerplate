(function(){

    var application = angular.module('boilerplate', ['controllers', 'services', 'directives', 'ui.router', 'ngCookies']);
    angular.module('controllers', []);
    angular.module('services', []);
    angular.module('directives', []);

    var apiUrl = 'http://manage.appscentral.co.uk/api/v1/';

    /* Routes */
    application.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider)
    {
        $stateProvider
            .state('/', {
                url         : "",
                templateUrl : "/views/index.html",
                controller  : 'DefaultController'
            })
            .state('schools', {
                url         : "/schools",
                templateUrl : "/views/school/index.html",
                controller  : 'SchoolController'
            })
            .state('schools-detail', {
                url         : "/schools/:id",
                templateUrl : "/views/school/detail.html",
                controller  : 'SchoolViewController'
            })
            .state('about', {
                url         : "/about",
                templateUrl : "/views/about.html",
                controller  : 'AboutController'
            })
            .state('contact', {
                url         : "/contact",
                templateUrl : "/views/contact.html",
                controller  : 'ContactController'
            })
            .state('dropdown-action', {
                url         : "/dropdown/action",
                templateUrl : "/views/dropdown/action.html",
                controller  : 'ActionController'
            })
            .state('dropdown-other-action', {
                url         : "/dropdown/other-action",
                templateUrl : "/views/dropdown/other-action.html",
                controller  : 'ActionController'
            });
    }]);

})();

