<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PasswordCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'password:make';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will create and store a password for a given user account.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$user = User::where('email', $this->argument('email'))->get()->first();
		if(null !== $this->argument('password') && !empty($this->argument('password'))) {
			$user->password = Hash::make($this->argument('password'));
		} else {
			$user->password = Hash::make($this->option('password'));
		}
		$user->save();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('email', InputArgument::REQUIRED, 'Enter the email of the user.',null),
			array('password', InputArgument::OPTIONAL, 'Define a password - optional', 'password'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
