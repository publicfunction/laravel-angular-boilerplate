<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
			// Auto Incementing ID
			$table->increments('id');

			// FK Lookup to Fishery Model
			$table->string('first_name', 200);
			$table->string('last_name', 200);
			$table->string('email', 250)->unique();
			$table->string('password', 255);

			$table->unsignedInteger('role_id');
			$table->foreign('role_id')
				->references('id')
				->on('roles');
			$table->boolean('active')->default(false);
			$table->string('remember_token')->nullable();
			$table->string('token')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
