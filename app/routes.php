<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
	return View::make('angular');
});

Route::get('api/v1/me', 'Tappleby\AuthToken\AuthTokenController@index');
Route::post('api/v1/login', 'Tappleby\AuthToken\AuthTokenController@store');
Route::post('api/v1/logout', 'Tappleby\AuthToken\AuthTokenController@destroy');

Route::api(['version' => 'v1', 'before' => 'auth.token'], function () {
	Route::group(['prefix' => 'api/v1', 'namespace' => 'Dummy\Controller\Api'], function () {
		Route::resource('', 'ApiController');
	});
});